package hu.webvalto.controller;

import hu.webvalto.dao.Employee;
import hu.webvalto.dao.EmployeeDao;
import lombok.Data;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 * @author Attila Budai <attila.budai@webvalto.hu>
 */
@Data
public class SalaryHandler {

    private EmployeeDao employeeDao;

    public SalaryHandler() {
        employeeDao = new EmployeeDao();
    }

    /**
     * A dolgozók fizetését frissítő metódus.
     * @param newSalary - az új fizetés értéke
     * @return Azokat a dolgozókat, amelyeknek nem sikerült a fizetését frissíteni.
     */
    public List<Employee> updateSalaries(Integer newSalary) {
        List<Employee> unmodifiedSalaries = new ArrayList<>();
        List<Employee> employees = employeeDao.findEmployees();
        employees.forEach(employee -> {
            employee.setSalary(newSalary);
            try {
                employeeDao.updateEmployee(employee);
            } catch (SQLException e) {
                unmodifiedSalaries.add(employee);
            }
        });
        return unmodifiedSalaries;
    }

}
