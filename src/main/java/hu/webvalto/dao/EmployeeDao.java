package hu.webvalto.dao;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Stream;

/**
 * @author Attila Budai <attila.budai@webvalto.hu>
 */
public class EmployeeDao extends BaseDao {

    public EmployeeDao() {
        try {
            createEmployeeTable();
        } catch (SQLException e) {
        }
    }

    public void createEmployeeTable() throws SQLException {
        try (Statement statement = getConnection().createStatement()) {
            statement.execute("create table if not exists Employee (id varchar2(36), name VARCHAR2(100), salary number(10) )");
        }
    }

    public void insert(Employee... employees) {
        Stream.of(employees).forEach(employee -> {
            try {
                insertEmployee(employee.getName(), employee.getSalary());
            } catch (SQLException e) {
            }
        });
    }


    public void insertEmployee(String name, Integer salary) throws SQLException {
        try (PreparedStatement preparedStatement = getConnection().prepareStatement("insert into employee values(?, ?, ?)")) {

            preparedStatement.setString(1, generateUUID());
            preparedStatement.setString(2, name);
            preparedStatement.setInt(3, salary);

            preparedStatement.execute();
        }
    }

    public void updateEmployee(Employee employee) throws SQLException {
        try (PreparedStatement preparedStatement = getConnection().prepareStatement("update employee set salary = ? where id = ?")) {

            preparedStatement.setInt(1, employee.getSalary());
            preparedStatement.setString(2, employee.getId());

            preparedStatement.execute();
        }
    }

    public List<Employee> findEmployees() {
        List<Employee> employeeNames = new ArrayList<>();
        try (Statement statement = getConnection().createStatement();
             ResultSet resultSet = statement.executeQuery("select id, name, salary from employee")) {

            while (resultSet.next()) {
                employeeNames.add(
                        Employee.builder()
                                .id(resultSet.getString("id"))
                                .name(resultSet.getString("name"))
                                .salary(resultSet.getInt("salary"))
                                .build()
                );
            }
        } catch (SQLException s) {
        }
        return employeeNames;
    }
}
