package hu.webvalto.dao;

import java.sql.Connection;
import java.sql.DriverManager;
import java.util.UUID;

/**
 * @author Attila Budai <attila.budai@webvalto.hu>
 */
public abstract class BaseDao {

    protected Connection getConnection() {
        try {
            Class.forName("org.h2.Driver");
            return DriverManager.getConnection("jdbc:h2:mem:test;MODE=Oracle", "sa", "");
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    protected String generateUUID() {
        return UUID.randomUUID().toString();
    }
}
