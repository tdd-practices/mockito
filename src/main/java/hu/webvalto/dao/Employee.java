package hu.webvalto.dao;

import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

/**
 * @author Attila Budai <attila.budai@webvalto.hu>
 */
@Data
@Builder
@ToString(of = {"id", "name", "salary"})
@EqualsAndHashCode(of = "id")
public class Employee {

    private String id;
    private String name;
    private Integer salary;
}
