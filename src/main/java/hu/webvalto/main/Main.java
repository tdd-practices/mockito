package hu.webvalto.main;

import hu.webvalto.controller.SalaryHandler;
import hu.webvalto.dao.EmployeeDao;

import java.sql.SQLException;

/**
 * @author Attila Budai <attila.budai@webvalto.hu>
 */
public class Main {

    public static void main(String[] args) throws ClassNotFoundException, SQLException {
        EmployeeDao employeeDao = new EmployeeDao();

        employeeDao.insertEmployee("Stan", 1200);

        employeeDao.findEmployees().forEach(System.out::println);

        employeeDao.findEmployees().forEach(employee ->
                new SalaryHandler().updateSalaries(2400)
        );
        employeeDao.findEmployees().forEach(System.out::println);
    }
}
