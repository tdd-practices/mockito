package hu.webvalto.dao;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.sql.SQLException;
import java.util.List;

import static org.junit.Assert.*;

/**
 * @author Péter Csubák <peter.csubak@webvalto.hu>
 */
public class EmployeeDaoTest {

    private EmployeeDao testObj;

    @Before
    public void setup() {
        testObj = new EmployeeDao();
    }

    @After
    public void teardown() throws SQLException {
        testObj.getConnection().createStatement().execute("drop table employee");
    }

    @Test
    public void should_save_entities1() {
        Employee employee1 = Employee.builder().name("Stan").salary(1200).build();
        Employee employee2 = Employee.builder().name("Not Stan").salary(1500).build();
        testObj.insert(employee1, employee2);

        List<Employee> employees = testObj.findEmployees();

//         Nem működik:  assertTrue(employees.containsAll(Arrays.asList(employee1, employee2)));
//        @EqualsAndHashCode(of = "id")
//        public class Employee {
//
//        hamcrest nélkül:

        employees.forEach(actualEmployee ->
                assertTrue(
                        actualEmployee.getName().equals("Stan") && actualEmployee.getSalary().equals(1200)
                                || actualEmployee.getName().equals("Not Stan") && actualEmployee.getSalary().equals(1500)
                )
        );
    }


}